import Details from './Details';
import Home from './Home';
import Speakers from './Speakers';

export default {
  Home,
  Details,
  Speakers,
};
